import static org.junit.Assert.*;

import org.junit.Test;

public class AT06E01ABaseTest {

	@Test
	public void verifierLignestest() {
	
		char[][] tab = {{ 'R', 'R', 'R', 'R', 'R', 'R', 'R',
						  'R', 'R', 'R', 'R', 'R', 'R', 'R', 
						  'R', 'R', 'R', 'R', 'R', 'R', 'R',
						  'R', 'R', 'R', 'R', 'R', 'R', 'R',
						  'R', 'R', 'R', 'R', 'R', 'R', 'R',
						  'B', 'B', 'B', 'B', 'R', 'R', 'R' }};
														  
														  
		assertEquals(false, AT06E01ABase.verifierLignes(tab));
		
		
		
			char[][] tab2 = {{ 'R', 'B', 'B', 'R', 'B', 'R', 'R',
							   'B', 'R', 'B', 'R', 'B', 'R', 'R', 
							   'R', 'B', 'R', 'B', 'R', 'B', 'R',
							   'R', 'R', 'B', 'B', 'R', 'R', 'B',
							   'B', 'R', 'R', 'R', 'B', 'R', 'B',
							   'B', 'B', 'B', 'B', 'R', 'B', 'R' }};
															  
															  
			assertEquals(false, AT06E01ABase.verifierLignes(tab2));
	}
	
	@Test
	public void verifierColonnestest() {
	
		char[][] tab = {{ 'R', 'R', 'R', 'R', 'R', 'R', 'R',
						  'R', 'R', 'R', 'R', 'R', 'R', 'R', 
						  'R', 'R', 'R', 'R', 'R', 'R', 'R',
						  'R', 'R', 'R', 'R', 'R', 'R', 'R',
						  'R', 'R', 'R', 'R', 'R', 'R', 'R',
						  'B', 'B', 'B', 'B', 'R', 'R', 'R' }};
	
		assertEquals(true, AT06E01ABase.verifierColonnes(tab));
	
		
		char[][] tab2 = {{ 'R', 'B', 'B', 'R', 'B', 'R', 'R',
			   			   'B', 'R', 'B', 'R', 'B', 'R', 'R', 
			   			   'R', 'B', 'R', 'B', 'R', 'B', 'R',
			   			   'R', 'R', 'B', 'B', 'R', 'R', 'B',
			   			   'B', 'R', 'R', 'R', 'B', 'R', 'B',
			   			   'B', 'B', 'B', 'B', 'R', 'B', 'R' }};

		assertEquals(true, AT06E01ABase.verifierColonnes(tab2));
	}
	
	@Test
	public void verifierDiagonalestest() {
	
		char[][] tab = {{ 'R', 'R', 'R', 'R', 'R', 'R', 'R',
						  'R', 'R', 'R', 'R', 'R', 'R', 'R', 
						  'R', 'R', 'R', 'R', 'R', 'R', 'R',
						  'R', 'R', 'R', 'R', 'R', 'R', 'R',
						  'R', 'R', 'R', 'R', 'R', 'R', 'R',
						  'B', 'B', 'B', 'B', 'R', 'R', 'R' }};
														  
														  
		assertEquals(false, AT06E01ABase.verifierDiagonales(tab));
		
	
		char[][] tab2 = {{ 'R', 'B', 'B', 'R', 'B', 'R', 'R',
			   			   'B', 'R', 'B', 'R', 'R', 'R', 'B', 
			   			   'R', 'B', 'R', 'B', 'R', 'B', 'R',
			   			   'R', 'R', 'B', 'B', 'R', 'R', 'B',
			   			   'B', 'B', 'R', 'R', 'B', 'R', 'B',
			   			   'B', 'B', 'B', 'B', 'R', 'B', 'R' }};

										  
										  
		assertEquals(false, AT06E01ABase.verifierDiagonales(tab2));
	}
	
	
}
